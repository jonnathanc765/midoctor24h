<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::truncate();

        factory(User::class)->create([
            'name' => 'Carlos Roa',
            'password' => bcrypt('password'),
            'email' => 'info@midoctor24h.com'
        ]);

    }
}
