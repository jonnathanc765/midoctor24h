<?php

namespace App\Http\Controllers;

use App\Doctor;
use Newsletter;
use App\Mail\DoctorContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Doctor::orderBy('id', 'DESC')->paginate(16);
        return view('backend.doctors.index', compact('doctors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'first_name' => 'required|string|min:3|max:191',
            'last_name' => 'required|string|min:3|max:191',
            'email' => 'required|string|email|max:191|unique:doctors,email',
            'phone' => 'required|numeric',
            'country' => 'required|string|min:3|max:191',
            'specialty' => 'required|string|max:191'
        ], [
            'emai.unique' => 'Este correo ya fue registrado, intenta con otro'
        ]);

        DB::beginTransaction();

        try {



            if ( ! Newsletter::isSubscribed($data['email']) ) {
                Newsletter::subscribe($data['email'], ['FNAME' => $data['first_name'], 'LNAME' => $data['last_name']]);
            }

            $doctor = Doctor::create([
                'name' => $data['first_name'] . ' ' . $data['last_name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'country' => $data['country'],
                'specialty' => $data['specialty']
            ]);

            Mail::to('info@midoctor24h.com')->send(new DoctorContactMail($doctor));

            DB::commit();

        } catch (\Throwable $th) {

            DB::rollBack();

            dd($th);

            return back()->withError('Ha ocurrido un error, intente de nuevo')->withInput($request->input());

        }

        return back()->withSuccess('Solicitud enviado exitosamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctor $doctor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctor $doctor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
        //
    }
}
