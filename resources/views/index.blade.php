<!DOCTYPE html>
<html lang="en">
<head>
	<title>MiDoctor24h</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset("vendor/bootstrap/css/bootstrap.min.css") }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset("fonts/font-awesome-4.7.0/css/font-awesome.min.css") }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset("fonts/Linearicons-Free-v1.0.0/icon-font.min.css") }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset("vendor/animate/animate.css") }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset("vendor/css-hamburgers/hamburgers.min.css") }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset("vendor/animsition/css/animsition.min.css") }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset("vendor/select2/select2.min.css") }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset("vendor/daterangepicker/daterangepicker.css") }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<form class="contact100-form validate-form" action="{{ route('doctor.store') }}" method="POST">
                @csrf
                <h3 class="d-block d-sm-none responsive-title text-center">Tu propio <span>consultorio online</span> con MiDoctor24h</h3>

				<span class="contact100-form-title">
					Contacta con nosotros
                </span>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

                @endif


				<div class="wrap-input100 validate-input" data-validate="El nombre es requerido">
					<label class="label-input100" for="first_name">Nombre</label>
					<input id="first_name" class="input100" value="{{ old('first_name') }}" type="text" name="first_name" placeholder="Ingresa tu nombre">
					<span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="El apellido es requerido">
					<label class="label-input100" for="last_name">Apellido</label>
					<input id="last_name" class="input100" value="{{ old('last_name') }}" type="text" name="last_name" placeholder="Ingresa tu apellido">
					<span class="focus-input100"></span>
				</div>


				<div class="wrap-input100 validate-input" data-validate = "Un correo válido es requerido: example@example.com">
					<label class="label-input100" for="email">Correo</label>
					<input id="email" class="input100" value="{{ old('email') }}" type="email" name="email" placeholder="Ingresa tu correo">
					<span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate = "Un telefono valido es requerido">
					<label class="label-input100" for="phone">Teléfono</label>
					<input id="phone" class="input100" value="{{ old('phone') }}" type="text" name="phone" placeholder="Ingresa tu teléfono...">
					<span class="focus-input100"></span>
                </div>

                <div class="wrap-input100">
					<div class="label-input100">¿Cual es tu país?</div>
					<div>
						<select class="js-select2" name="country">
                            <option value="" selected id="AF">Elegir opción</option>
                            <option value="Afganistán" id="AF">Afganistán</option>
                            <option value="Albania" id="AL">Albania</option>
                            <option value="Alemania" id="DE">Alemania</option>
                            <option value="Andorra" id="AD">Andorra</option>
                            <option value="Angola" id="AO">Angola</option>
                            <option value="Anguila" id="AI">Anguila</option>
                            <option value="Antártida" id="AQ">Antártida</option>
                            <option value="Antigua y Barbuda" id="AG">Antigua y Barbuda</option>
                            <option value="Antillas holandesas" id="AN">Antillas holandesas</option>
                            <option value="Arabia Saudí" id="SA">Arabia Saudí</option>
                            <option value="Argelia" id="DZ">Argelia</option>
                            <option value="Argentina" id="AR">Argentina</option>
                            <option value="Armenia" id="AM">Armenia</option>
                            <option value="Aruba" id="AW">Aruba</option>
                            <option value="Australia" id="AU">Australia</option>
                            <option value="Austria" id="AT">Austria</option>
                            <option value="Azerbaiyán" id="AZ">Azerbaiyán</option>
                            <option value="Bahamas" id="BS">Bahamas</option>
                            <option value="Bahrein" id="BH">Bahrein</option>
                            <option value="Bangladesh" id="BD">Bangladesh</option>
                            <option value="Barbados" id="BB">Barbados</option>
                            <option value="Bélgica" id="BE">Bélgica</option>
                            <option value="Belice" id="BZ">Belice</option>
                            <option value="Benín" id="BJ">Benín</option>
                            <option value="Bermudas" id="BM">Bermudas</option>
                            <option value="Bhután" id="BT">Bhután</option>
                            <option value="Bielorrusia" id="BY">Bielorrusia</option>
                            <option value="Birmania" id="MM">Birmania</option>
                            <option value="Bolivia" id="BO">Bolivia</option>
                            <option value="Bosnia y Herzegovina" id="BA">Bosnia y Herzegovina</option>
                            <option value="Botsuana" id="BW">Botsuana</option>
                            <option value="Brasil" id="BR">Brasil</option>
                            <option value="Brunei" id="BN">Brunei</option>
                            <option value="Bulgaria" id="BG">Bulgaria</option>
                            <option value="Burkina Faso" id="BF">Burkina Faso</option>
                            <option value="Burundi" id="BI">Burundi</option>
                            <option value="Cabo Verde" id="CV">Cabo Verde</option>
                            <option value="Camboya" id="KH">Camboya</option>
                            <option value="Camerún" id="CM">Camerún</option>
                            <option value="Canadá" id="CA">Canadá</option>
                            <option value="Chad" id="TD">Chad</option>
                            <option value="Chile" id="CL">Chile</option>
                            <option value="China" id="CN">China</option>
                            <option value="Chipre" id="CY">Chipre</option>
                            <option value="Ciudad estado del Vaticano" id="VA">Ciudad estado del Vaticano</option>
                            <option value="Colombia" id="CO">Colombia</option>
                            <option value="Comores" id="KM">Comores</option>
                            <option value="Congo" id="CG">Congo</option>
                            <option value="Corea" id="KR">Corea</option>
                            <option value="Corea del Norte" id="KP">Corea del Norte</option>
                            <option value="Costa del Marfíl" id="CI">Costa del Marfíl</option>
                            <option value="Costa Rica" id="CR">Costa Rica</option>
                            <option value="Croacia" id="HR">Croacia</option>
                            <option value="Cuba" id="CU">Cuba</option>
                            <option value="Dinamarca" id="DK">Dinamarca</option>
                            <option value="Djibouri" id="DJ">Djibouri</option>
                            <option value="Dominica" id="DM">Dominica</option>
                            <option value="Ecuador" id="EC">Ecuador</option>
                            <option value="Egipto" id="EG">Egipto</option>
                            <option value="El Salvador" id="SV">El Salvador</option>
                            <option value="Emiratos Arabes Unidos" id="AE">Emiratos Arabes Unidos</option>
                            <option value="Eritrea" id="ER">Eritrea</option>
                            <option value="Eslovaquia" id="SK">Eslovaquia</option>
                            <option value="Eslovenia" id="SI">Eslovenia</option>
                            <option value="España" id="ES">España</option>
                            <option value="Estados Unidos" id="US">Estados Unidos</option>
                            <option value="Estonia" id="EE">Estonia</option>
                            <option value="c" id="ET">Etiopía</option>
                            <option value="Ex-República Yugoslava de Macedonia" id="MK">Ex-República Yugoslava de Macedonia</option>
                            <option value="Filipinas" id="PH">Filipinas</option>
                            <option value="Finlandia" id="FI">Finlandia</option>
                            <option value="Francia" id="FR">Francia</option>
                            <option value="Gabón" id="GA">Gabón</option>
                            <option value="Gambia" id="GM">Gambia</option>
                            <option value="Georgia" id="GE">Georgia</option>
                            <option value="Georgia del Sur y las islas Sandwich del Sur" id="GS">Georgia del Sur y las islas Sandwich del Sur</option>
                            <option value="Ghana" id="GH">Ghana</option>
                            <option value="Gibraltar" id="GI">Gibraltar</option>
                            <option value="Granada" id="GD">Granada</option>
                            <option value="Grecia" id="GR">Grecia</option>
                            <option value="Groenlandia" id="GL">Groenlandia</option>
                            <option value="Guadalupe" id="GP">Guadalupe</option>
                            <option value="Guam" id="GU">Guam</option>
                            <option value="Guatemala" id="GT">Guatemala</option>
                            <option value="Guayana" id="GY">Guayana</option>
                            <option value="Guayana francesa" id="GF">Guayana francesa</option>
                            <option value="Guinea" id="GN">Guinea</option>
                            <option value="Guinea Ecuatorial" id="GQ">Guinea Ecuatorial</option>
                            <option value="Guinea-Bissau" id="GW">Guinea-Bissau</option>
                            <option value="Haití" id="HT">Haití</option>
                            <option value="Holanda" id="NL">Holanda</option>
                            <option value="Honduras" id="HN">Honduras</option>
                            <option value="Hong Kong R. A. E" id="HK">Hong Kong R. A. E</option>
                            <option value="Hungría" id="HU">Hungría</option>
                            <option value="India" id="IN">India</option>
                            <option value="Indonesia" id="ID">Indonesia</option>
                            <option value="Irak" id="IQ">Irak</option>
                            <option value="Irán" id="IR">Irán</option>
                            <option value="Irlanda" id="IE">Irlanda</option>
                            <option value="Isla Bouvet" id="BV">Isla Bouvet</option>
                            <option value="Isla Christmas" id="CX">Isla Christmas</option>
                            <option value="Isla Heard e Islas McDonald" id="HM">Isla Heard e Islas McDonald</option>
                            <option value="Islandia" id="IS">Islandia</option>
                            <option value="Islas Caimán" id="KY">Islas Caimán</option>
                            <option value="Islas Cook" id="CK">Islas Cook</option>
                            <option value="Islas de Cocos o Keeling" id="CC">Islas de Cocos o Keeling</option>
                            <option value="Islas Faroe" id="FO">Islas Faroe</option>
                            <option value="Islas Fiyi" id="FJ">Islas Fiyi</option>
                            <option value="Islas Malvinas Islas Falkland" id="FK">Islas Malvinas Islas Falkland</option>
                            <option value="Islas Marianas del norte" id="MP">Islas Marianas del norte</option>
                            <option value="Islas Marshall" id="MH">Islas Marshall</option>
                            <option value="Islas menores de Estados Unidos" id="UM">Islas menores de Estados Unidos</option>
                            <option value="Islas Palau" id="PW">Islas Palau</option>
                            <option value="Islas Salomón" d="SB">Islas Salomón</option>
                            <option value="Islas Tokelau" id="TK">Islas Tokelau</option>
                            <option value="Islas Turks y Caicos" id="TC">Islas Turks y Caicos</option>
                            <option value="Islas Vírgenes EE.UU." id="VI">Islas Vírgenes EE.UU.</option>
                            <option value="Islas Vírgenes Reino Unido" id="VG">Islas Vírgenes Reino Unido</option>
                            <option value="Israel" id="IL">Israel</option>
                            <option value="Italia" id="IT">Italia</option>
                            <option value="Jamaica" id="JM">Jamaica</option>
                            <option value="Japón" id="JP">Japón</option>
                            <option value="Jordania" id="JO">Jordania</option>
                            <option value="Kazajistán" id="KZ">Kazajistán</option>
                            <option value="Kenia" id="KE">Kenia</option>
                            <option value="Kirguizistán" id="KG">Kirguizistán</option>
                            <option value="Kiribati" id="KI">Kiribati</option>
                            <option value="Kuwait" id="KW">Kuwait</option>
                            <option value="Laos" id="LA">Laos</option>
                            <option value="Lesoto" id="LS">Lesoto</option>
                            <option value="Letonia" id="LV">Letonia</option>
                            <option value="Líbano" id="LB">Líbano</option>
                            <option value="Liberia" id="LR">Liberia</option>
                            <option value="Libia" id="LY">Libia</option>
                            <option value="Liechtenstein" id="LI">Liechtenstein</option>
                            <option value="Lituania" id="LT">Lituania</option>
                            <option value="Luxemburgo" id="LU">Luxemburgo</option>
                            <option value="Macao R. A. E" id="MO">Macao R. A. E</option>
                            <option value="Madagascar" id="MG">Madagascar</option>
                            <option value="Malasia" id="MY">Malasia</option>
                            <option value="Malawi" id="MW">Malawi</option>
                            <option value="Maldivas" id="MV">Maldivas</option>
                            <option value="Malí" id="ML">Malí</option>
                            <option value="Malta" id="MT">Malta</option>
                            <option value="Marruecos" id="MA">Marruecos</option>
                            <option value="Martinica" id="MQ">Martinica</option>
                            <option value="Mauricio" id="MU">Mauricio</option>
                            <option value="Mauritania" id="MR">Mauritania</option>
                            <option value="Mayotte" id="YT">Mayotte</option>
                            <option value="México" id="MX">México</option>
                            <option value="Micronesia" id="FM">Micronesia</option>
                            <option value="Moldavia" id="MD">Moldavia</option>
                            <option value="Mónaco" id="MC">Mónaco</option>
                            <option value="Mongolia" id="MN">Mongolia</option>
                            <option value="Montserrat" id="MS">Montserrat</option>
                            <option value="Mozambique" id="MZ">Mozambique</option>
                            <option value="Namibia" id="NA">Namibia</option>
                            <option value="Nauru" id="NR">Nauru</option>
                            <option value="Nepal" id="NP">Nepal</option>
                            <option value="Nicaragua" id="NI">Nicaragua</option>
                            <option value="Níger" id="NE">Níger</option>
                            <option value="Nigeria" id="NG">Nigeria</option>
                            <option value="Niue" id="NU">Niue</option>
                            <option value="Norfolk" id="NF">Norfolk</option>
                            <option value="Noruega" id="NO">Noruega</option>
                            <option value="Nueva Caledonia" id="NC">Nueva Caledonia</option>
                            <option value="Nueva Zelanda" id="NZ">Nueva Zelanda</option>
                            <option value="Omán" id="OM">Omán</option>
                            <option value="Panamá" id="PA">Panamá</option>
                            <option value="Papua Nueva Guinea" id="PG">Papua Nueva Guinea</option>
                            <option value="Paquistán" id="PK">Paquistán</option>
                            <option value="Paraguay" id="PY">Paraguay</option>
                            <option value="Perú" id="PE">Perú</option>
                            <option value="Pitcairn" id="PN">Pitcairn</option>
                            <option value="Polinesia francesa" id="PF">Polinesia francesa</option>
                            <option value="Polonia" id="PL">Polonia</option>
                            <option value="Portugal" id="PT">Portugal</option>
                            <option value="Puerto Rico" id="PR">Puerto Rico</option>
                            <option value="Qatar" id="QA">Qatar</option>
                            <option value="Reino Unido" id="UK">Reino Unido</option>
                            <option value="República Centroafricana" id="CF">República Centroafricana</option>
                            <option value="República Checa" id="CZ">República Checa</option>
                            <option value="República de Sudáfrica" id="ZA">República de Sudáfrica</option>
                            <option value="República Democrática del Congo Zaire" id="CD">República Democrática del Congo Zaire</option>
                            <option value="República Dominicana" id="DO">República Dominicana</option>
                            <option value="Reunión" id="RE">Reunión</option>
                            <option value="Ruanda" id="RW">Ruanda</option>
                            <option value="Rumania" id="RO">Rumania</option>
                            <option value="Rusia" id="RU">Rusia</option>
                            <option value="Samoa" id="WS">Samoa</option>
                            <option value="Samoa occidental" id="AS">Samoa occidental</option>
                            <option value="San Kitts y Nevis" id="KN">San Kitts y Nevis</option>
                            <option value="San Marino" id="SM">San Marino</option>
                            <option value="San Pierre y Miquelon" id="PM">San Pierre y Miquelon</option>
                            <option value="San Vicente e Islas Granadinas" id="VC">San Vicente e Islas Granadinas</option>
                            <option value="Santa Helena" id="SH">Santa Helena</option>
                            <option value="Santa Lucía" id="LC">Santa Lucía</option>
                            <option value="Santo Tomé y Príncipe" id="ST">Santo Tomé y Príncipe</option>
                            <option value="Senegal" id="SN">Senegal</option>
                            <option value="Serbia y Montenegro" id="YU">Serbia y Montenegro</option>
                            <option value="Sychelles" id="SC">Seychelles</option>
                            <option value="Sierra Leona" id="SL">Sierra Leona</option>
                            <option value="Singapur" id="SG">Singapur</option>
                            <option value="Siria" id="SY">Siria</option>
                            <option value="Somalia" id="SO">Somalia</option>
                            <option value="Sri Lanka" id="LK">Sri Lanka</option>
                            <option value="Suazilandia" id="SZ">Suazilandia</option>
                            <option value="Sudán" id="SD">Sudán</option>
                            <option value="Suecia" id="SE">Suecia</option>
                            <option value="Suiza" id="CH">Suiza</option>
                            <option value="Surinam" id="SR">Surinam</option>
                            <option value="Svalbard" id="SJ">Svalbard</option>
                            <option value="Tailandia" id="TH">Tailandia</option>
                            <option value="Taiwán" id="TW">Taiwán</option>
                            <option value="Tanzania" id="TZ">Tanzania</option>
                            <option value="Tayikistán" id="TJ">Tayikistán</option>
                            <option value="Territorios británicos del océano Indico" id="IO">Territorios británicos del océano Indico</option>
                            <option value="Territorios franceses del sur" id="TF">Territorios franceses del sur</option>
                            <option value="Timor Oriental" id="TP">Timor Oriental</option>
                            <option value="Togo" id="TG">Togo</option>
                            <option value="Tonga" id="TO">Tonga</option>
                            <option value="Trinidad y Tobago" id="TT">Trinidad y Tobago</option>
                            <option value="Túnez" id="TN">Túnez</option>
                            <option value="Turkmenistán" id="TM">Turkmenistán</option>
                            <option value="Turquía" id="TR">Turquía</option>
                            <option value="Tuvalu" id="TV">Tuvalu</option>
                            <option value="Ucrania" id="UA">Ucrania</option>
                            <option value="Uganda" id="UG">Uganda</option>
                            <option value="Uruguay" id="UY">Uruguay</option>
                            <option value="Uzbekistán" id="UZ">Uzbekistán</option>
                            <option value="Vanuatu" id="VU">Vanuatu</option>
                            <option value="Venezuela" id="VE">Venezuela</option>
                            <option value="Vietnam" id="VN">Vietnam</option>
                            <option value="Wallis y Futuna" id="WF">Wallis y Futuna</option>
                            <option value="Yemen" id="YE">Yemen</option>
                            <option value="Zambia" id="ZM">Zambia</option>
                            <option value="Zimbabue" id="ZW">Zimbabue</option>
						</select>
						<div class="dropDownSelect2"></div>
					</div>
					<span class="focus-input100"></span>
				</div>



				<div class="wrap-input100">
					<div class="label-input100">¿Cual es tu especialidad?</div>
					<div>
						<select class="js-select2" name="specialty">
                            <option value=""selected="selected">Especialidad</option>
                            <option value="Abuso Infantil Pediatría">Abuso Infantil Pediatría</option>
                            <option value="Acupuntura Científica">Acupuntura Científica</option>
                            <option value="Alergia pediátrica, inmunología y enfermedades infecciosas">Alergia pediátrica, inmunología y enfermedades infecciosas</option>
                            <option value="Andrologia">Andrologia</option>
                            <option value="Anestesiología">Anestesiología</option>
                            <option value="Anestesiología Pediátrica">Anestesiología Pediátrica</option>
                            <option value="Angiología / Medicina Vascular">Angiología / Medicina Vascular</option>
                            <option value="Banco de sangre / medicina de transfusión">Banco de sangre / medicina de transfusión</option>
                            <option value="Cardiología">Cardiología</option>
                            <option value="Cardiología Intervencionista">Cardiología Intervencionista</option>
                            <option value="Cardiología Nuclear">Cardiología Nuclear</option>
                            <option value="Cardiología pediátrica">Cardiología pediátrica</option>
                            <option value="Catarata y Cirugía Refractiva">Catarata y Cirugía Refractiva</option>
                            <option value="Cirugía bariátrica">Cirugía bariátrica</option>
                            <option value="Cirugía cardíaca para adultos">Cirugía cardíaca para adultos</option>
                            <option value="Cirugía cardiotorácica">Cirugía cardiotorácica</option>
                            <option value="Cirugía Colorrectal">Cirugía Colorrectal</option>
                            <option value="Cirugía cosmética">Cirugía cosmética</option>
                            <option value="Cirugía Craneofacial" >Cirugía Craneofacial</option>
                            <option value="Cirugía de base de cráneo">Cirugía de base de cráneo</option>
                            <option value="Cirugía de busto">Cirugía de busto</option>
                            <option value="Cirugía de columna">Cirugía de columna</option>
                            <option value="Cirugía de cuidados críticos">Cirugía de cuidados críticos</option>
                            <option value="Cirugía de hombro y codo">Cirugía de hombro y codo</option>
                            <option value="Cirugía de la columna">Cirugía de la columna</option>
                            <option value="Cirugía de la mano">Cirugía de la mano</option>
                            <option value="Cirugía de mano">Cirugía de mano</option>
                            <option value="Cirugía de mano y extremidades superiores">Cirugía de mano y extremidades superiores</option>
                            <option value="Cirugía de Mohs">Cirugía de Mohs</option>
                            <option value="Cirugía de pie y tobillo">Cirugía de pie y tobillo</option>
                            <option value="Cirugía de piel">Cirugía de piel</option>
                            <option value="Cirugía de quemaduras">Cirugía de quemaduras</option>
                            <option value="Cirugía de reconstrucción articular">Cirugía de reconstrucción articular</option>
                            <option value="Cirugía de trasplante">Cirugía de trasplante</option>
                            <option value="Cirugía Endocrina">Cirugía Endocrina</option>
                            <option value="Cirugía Esofagogástrica">Cirugía Esofagogástrica</option>
                            <option value="Cirugía Estética / Cosmética">Cirugía Estética / Cosmética</option>
                            <option value="Cirugía Estética Facial">Cirugía Estética Facial</option>
                            <option value="Cirugía General">Cirugía General</option>
                            <option value="Cirugía hepato-biliar-pancreática">Cirugía hepato-biliar-pancreática</option>
                            <option value="Cirugía laparoscópica">Cirugía laparoscópica</option>
                            <option value="Cirugía Neonatal">Cirugía Neonatal</option>
                            <option value="Cirugía Oncológica de Cabeza y Cuello">Cirugía Oncológica de Cabeza y Cuello</option>
                            <option value="Cirugía Oral y Maxilofacial">Cirugía Oral y Maxilofacial</option>
                            <option value="Cirugía ortopedica">Cirugía ortopedica</option>
                            <option value="Cirugía pediátrica">Cirugía pediátrica</option>
                            <option value="Cirugía plástica">Cirugía plástica</option>
                            <option value="Cirugía plástica facial y reconstructiva">Cirugía plástica facial y reconstructiva</option>
                            <option value="Cirugía plástica oftálmica">Cirugía plástica oftálmica</option>
                            <option value="Cirugía Torácica">Cirugía Torácica</option>
                            <option value="Cirugía vascular">Cirugía vascular</option>
                            <option value="Citogenética Clínica">Citogenética Clínica</option>
                            <option value="Citopatología">Citopatología</option>
                            <option value="Consultas y Ayudas Espirituales">Consultas y Ayudas Espirituales</option>
                            <option value="Córnea y enfermedad externa">Córnea y enfermedad externa</option>
                            <option value="Dermatología">Dermatología</option>
                            <option value="Dermatologia Cosmetica">Dermatologia Cosmetica</option>
                            <option value="Dermatología pediátrica">Dermatología pediátrica</option>
                            <option value="Dermatopatología">Dermatopatología</option>
                            <option value="Diabetes pediátrica y endocrinología">Diabetes pediátrica y endocrinología</option>
                            <option value="Diabetologia">Diabetologia</option>
                            <option value="Diagnóstico Física Médica">Diagnóstico Física Médica</option>
                            <option value="Discapacidades del neurodesarrollo">Discapacidades del neurodesarrollo</option>
                            <option value="Ecocardiografía">Ecocardiografía</option>
                            <option value="Electrofisiología cardíaca clínica">Electrofisiología cardíaca clínica</option>
                            <option value="Endocrinología">Endocrinología</option>
                            <option value="Endodoncia">Endodoncia</option>
                            <option value="Endourología">Endourología</option>
                            <option value="Enfermedad cardíaca congénita del adulto">Enfermedad cardíaca congénita del adulto</option>
                            <option value="Enfermedades infecciosas">Enfermedades infecciosas</option>
                            <option value="Enfermedades infecciosas pediátricas">Enfermedades infecciosas pediátricas</option>
                            <option value="Enfermedades Vitreorretinianas">Enfermedades Vitreorretinianas</option>
                            <option value="Enlace Psiquiatría / Medicina Psicosomática">Enlace Psiquiatría / Medicina Psicosomática</option>
                            <option value="Epilepsia">Epilepsia</option>
                            <option value="Farmacología Clínica Pediátrica y Terapéutica">Farmacología Clínica Pediátrica y Terapéutica</option>
                            <option value="Farmacología Clínica y Terapéutica">Farmacología Clínica y Terapéutica</option>
                            <option value="Fisiatría de intervención">Fisiatría de intervención</option>
                            <option value="Física Médica">Física Médica</option>
                            <option value="Física Médica Terapéutica">Física Médica Terapéutica</option>
                            <option value="Física Nuclear Médica">Física Nuclear Médica</option>
                            <option value="Fisiología">Fisiología</option>
                            <option value="Fisioterapia">Fisioterapia</option>
                            <option value="Fisioterapia">Fisioterapia</option>
                            <option value="Gastroenterología">Gastroenterología</option>
                            <option value="Gastroenterología Quirúrgica / Cirugía Gastrointestinal">Gastroenterología Quirúrgica / Cirugía Gastrointestinal</option>
                            <option value="Gastroenterología, Hepatología y Nutrición Pediátrica">Gastroenterología, Hepatología y Nutrición Pediátrica</option>
                            <option value="Genética Bioquímica Clínica">Genética Bioquímica Clínica</option>
                            <option value="Genética Clínica">Genética Clínica</option>
                            <option value="Genética Médica">Genética Médica</option>
                            <option value="Genética Mitocondrial Clínica">Genética Mitocondrial Clínica</option>
                            <option value="Genética Molecular Clínica">Genética Molecular Clínica</option>
                            <option value="Genética Molecular Patología">Genética Molecular Patología</option>
                            <option value="Genética Ocular">Genética Ocular</option>
                            <option value="Geriatría / Medicina Geriátrica">Geriatría / Medicina Geriátrica</option>
                            <option value="Geropsicologia Clinica">Geropsicologia Clinica</option>
                            <option value="Ginecología y Obstetricia">Ginecología y Obstetricia</option>
                            <option value="Glaucoma">Glaucoma</option>
                            <option value="Hematología">Hematología</option>
                            <option value="Hematología pediátrica">Hematología pediátrica</option>
                            <option value="Hematología y oncología pediátrica">Hematología y oncología pediátrica</option>
                            <option value="Hematopatología">Hematopatología</option>
                            <option value="Hepatología / Medicina Hepatobiliar">Hepatología / Medicina Hepatobiliar</option>
                            <option value="Hepatología de trasplante">Hepatología de trasplante</option>
                            <option value="Hepatología de trasplante pediátrico">Hepatología de trasplante pediátrico</option>
                            <option value="Homeopatía">Homeopatía</option>
                            <option value="Hospicio y medicina paliativa">Hospicio y medicina paliativa</option>
                            <option value="Informática clínica">Informática clínica</option>
                            <option value="Informática de salud">Informática de salud</option>
                            <option value="Inmunodermatología">Inmunodermatología</option>
                            <option value="Inmunología">Inmunología</option>
                            <option value="Inmunología clínica">Inmunología clínica</option>
                            <option value="Insuficiencia cardíaca avanzada y cardiología de trasplante">Insuficiencia cardíaca avanzada y cardiología de trasplante</option>
                            <option value="Investigación Biomédica / Medicina Experimental">Investigación Biomédica / Medicina Experimental</option>
                            <option value="Laringiología">Laringiología</option>
                            <option value="Medicina adolescente">Medicina adolescente</option>
                            <option value="Medicina aeroespacial">Medicina aeroespacial</option>
                            <option value="Medicina Alternativa">Medicina Alternativa</option>
                            <option value="Medicina audiovestibular">Medicina audiovestibular</option>
                            <option value="Medicina de atención urgente">Medicina de atención urgente</option>
                            <option value="Medicina de Cuidados Críticos / Medicina de Cuidados Intensivos">Medicina de Cuidados Críticos / Medicina de Cuidados Intensivos</option>
                            <option value="Medicina de Cuidados Intensivos">Medicina de Cuidados Intensivos</option>
                            <option value="Medicina de desastres">Medicina de desastres</option>
                            <option value="Medicina de emergencia">Medicina de emergencia</option>
                            <option value="Medicina de emergencia pediátrica">Medicina de emergencia pediátrica</option>
                            <option value="Medicina de emergencia prehospitalaria">Medicina de emergencia prehospitalaria</option>
                            <option value="Medicina de la adicción">Medicina de la adicción</option>
                            <option value="Medicina de laboratorio">Medicina de laboratorio</option>
                            <option value="Medicina de lesiones cerebrales">Medicina de lesiones cerebrales</option>
                            <option value="Medicina de lesiones de la médula espinal">Medicina de lesiones de la médula espinal</option>
                            <option value="Medicina de rehabilitación pediátrica">Medicina de rehabilitación pediátrica</option>
                            <option value="Medicina de viaje">Medicina de viaje</option>
                            <option value="Medicina del desierto">Medicina del desierto</option>
                            <option value="Medicina del hospital">Medicina del hospital</option>
                            <option value="Medicina del sueño">Medicina del sueño</option>
                            <option value="Medicina deportiva">Medicina deportiva</option>
                            <option value="Medicina Deportiva Pediátrica">Medicina Deportiva Pediátrica</option>
                            <option value="Medicina Deportiva Pediátrica">Medicina Deportiva Pediátrica</option>
                            <option value="Medicina deportiva quirúrgica">Medicina deportiva quirúrgica</option>
                            <option value="Medicina Familiar">Medicina Familiar</option>
                            <option value="Medicina farmacéutica">Medicina farmacéutica</option>
                            <option value="Medicina Física y Rehabilitación / Fisiatría">Medicina Física y Rehabilitación / Fisiatría</option>
                            <option value="Medicina Forense">Medicina Forense</option>
                            <option value="Medicina Genitourinaria (Salud Sexual y Medicina del VIH) / Venereología">Medicina Genitourinaria (Salud Sexual y Medicina del VIH) / Venereología</option>
                            <option value="Medicina Interna">Medicina Interna</option>
                            <option value="Medicina materna y fetal">Medicina materna y fetal</option>
                            <option value="Medicina metabólica hereditaria pediátrica">Medicina metabólica hereditaria pediátrica</option>
                            <option value="Medicina neuromuscular">Medicina neuromuscular</option>
                            <option value="Medicina Nuclear">Medicina Nuclear</option>
                            <option value="Medicina Ocupacional">Medicina Ocupacional</option>
                            <option value="Medicina Oral">Medicina Oral</option>
                            <option value="Medicina paliativa pediátrica">Medicina paliativa pediátrica</option>
                            <option value="Medicina para el derrame cerebral">Medicina para el derrame cerebral</option>
                            <option value="Medicina para el dolor">Medicina para el dolor</option>
                            <option value="Medicina Pediátrica de Cuidados Intensivos">Medicina Pediátrica de Cuidados Intensivos</option>
                            <option value="Medicina Pediátrica de Enfermedades Infecciosas">Medicina Pediátrica de Enfermedades Infecciosas</option>
                            <option value="Medicina pediátrica del sueño">Medicina pediátrica del sueño</option>
                            <option value="Medicina pélvica femenina y cirugía reconstructiva">Medicina pélvica femenina y cirugía reconstructiva</option>
                            <option value="Medicina Preventiva">Medicina Preventiva</option>
                            <option value="Medicina reproductiva">Medicina reproductiva</option>
                            <option value="Medicina Respiratoria Pediátrica">Medicina Respiratoria Pediátrica</option>
                            <option value="Medicina sexual">Medicina sexual</option>
                            <option value="Medicina Siddha">Medicina Siddha</option>
                            <option value="Medicina submarina e hiperbárica">Medicina submarina e hiperbárica</option>
                            <option value="Medicina tradicional china">Medicina tradicional china</option>
                            <option value="Medicina Tropical">Medicina Tropical</option>
                            <option value="Medicina Unani">Medicina Unani</option>
                            <option value="Médico Alergologo">Médico Alergologo</option>
                            <option value="Médico general">Médico general</option>
                            <option value="Médico pediátra">Médico pediátra</option>
                            <option value="Microbiología médica">Microbiología médica</option>
                            <option value="Microcirugía">Microcirugía</option>
                            <option value="Nefrología">Nefrología</option>
                            <option value="Nefrología de trasplante">Nefrología de trasplante</option>
                            <option value="Nefrología Intervencionista">Nefrología Intervencionista</option>
                            <option value="Nefrología pediátrica">Nefrología pediátrica</option>
                            <option value="Neonatología / Medicina Neonatal">Neonatología / Medicina Neonatal</option>
                            <option value="Neumología / Medicina Respiratoria">Neumología / Medicina Respiratoria</option>
                            <option value="Neumología Intervencionista">Neumología Intervencionista</option>
                            <option value="Neumología pediátrica">Neumología pediátrica</option>
                            <option value="Neuro Psiquiatría">Neuro Psiquiatría</option>
                            <option value="Neurocirugía">Neurocirugía</option>
                            <option value="Neurocirugía Funcional">Neurocirugía Funcional</option>
                            <option value="Neurocirugía pediátrica">Neurocirugía pediátrica</option>
                            <option value="Neurocirugía vascular y endovascular">Neurocirugía vascular y endovascular</option>
                            <option value="Neurodisabilidad pediátrica">Neurodisabilidad pediátrica</option>
                            <option value="Neurofisiología clínica">Neurofisiología clínica</option>
                            <option value="Neurología">Neurología</option>
                            <option value="Neurología Intervencionista">Neurología Intervencionista</option>
                            <option value="Neurología pediátrica">Neurología pediátrica</option>
                            <option value="Neurología Vascular">Neurología Vascular</option>
                            <option value="Neurooftalmología">Neurooftalmología</option>
                            <option value="Neuropatología">Neuropatología</option>
                            <option value="Neuropsicología clínica">Neuropsicología clínica</option>
                            <option value="Neurorradiología">Neurorradiología</option>
                            <option value="Neurotologia">Neurotologia</option>
                            <option value="Nutrición y Dietética">Nutrición y Dietética</option>
                            <option value="Odontología de salud pública">Odontología de salud pública</option>
                            <option value="Odontología general">Odontología general</option>
                            <option value="Odontología Pediatrica">Odontología Pediatrica</option>
                            <option value="Oftalmología">Oftalmología</option>
                            <option value="Oftalmología pediátrica">Oftalmología pediátrica</option>
                            <option value="Oncología ginecológica">Oncología ginecológica</option>
                            <option value="Oncologia medica">Oncologia medica</option>
                            <option value="Oncología musculoesquelética">Oncología musculoesquelética</option>
                            <option value="Oncología neuroquirúrgica">Oncología neuroquirúrgica</option>
                            <option value="Oncología ocular">Oncología ocular</option>
                            <option value="Oncología ortopédica">Oncología ortopédica</option>
                            <option value="Oncología pediátrica">Oncología pediátrica</option>
                            <option value="Oncología quirúrgica">Oncología quirúrgica</option>
                            <option value="Oncología Radioterápica">Oncología Radioterápica</option>
                            <option value="Oncología urológica">Oncología urológica</option>
                            <option value="ORL pediatrico">ORL pediatrico</option>
                            <option value="Ortodoncia y Ortopedia Dentofacial">Ortodoncia y Ortopedia Dentofacial</option>
                            <option value="Ortopedía">Ortopedía</option>
                            <option value="Ortopedia Pediátrica">Ortopedia Pediátrica</option>
                            <option value="Otorrinolaringología">Otorrinolaringología</option>
                            <option value="Otorrinolaringología pediátrica">Otorrinolaringología pediátrica</option>
                            <option value="Patología">Patología</option>
                            <option value="Patología Anatómica">Patología Anatómica</option>
                            <option value="Patología del habla y Lenguaje">Patología del habla y Lenguaje</option>
                            <option value="Patología Forense">Patología Forense</option>
                            <option value="Patología general">Patología general</option>
                            <option value="Patología Oftálmica">Patología Oftálmica</option>
                            <option value="Patología Oral y Maxilofacial">Patología Oral y Maxilofacial</option>
                            <option value="Patología pediátrica / Patología pediátrica y perinatal">Patología pediátrica / Patología pediátrica y perinatal</option>
                            <option value="Patología pediátrica y perinatal">Patología pediátrica y perinatal</option>
                            <option value="Patología Química">Patología Química</option>
                            <option value="Patología Química / Bioquímica Clínica">Patología Química / Bioquímica Clínica</option>
                            <option value="Pediatría">Pediatría</option>
                            <option value="Pediatría del comportamiento conductual">Pediatría del comportamiento conductual</option>
                            <option value="Periodoncia">Periodoncia</option>
                            <option value="Podología">Podología</option>
                            <option value="Prostodoncia">Prostodoncia</option>
                            <option value="Psicología clínica">Psicología clínica</option>
                            <option value="Psicología del deporte">Psicología del deporte</option>
                            <option value="Psicologia Escolar">Psicologia Escolar</option>
                            <option value="Psicología Espiritual">Psicología Espiritual</option>
                            <option value="Psicología Forense">Psicología Forense</option>
                            <option value="Psicología industrial y organizacional / Psicología del trabajo / Psicología ">Psicología industrial y organizacional / Psicología del trabajo / Psicología </option>
                            <option value="Psicologia medica">Psicologia medica</option>
                            <option value="Psicopatología Infantil">Psicopatología Infantil</option>
                            <option value="Psicoterapia médica">Psicoterapia médica</option>
                            <option value="Psicoterapia médica">Psicoterapia médica</option>
                            <option value="Psiquiatría de adicciones">Psiquiatría de adicciones</option>
                            <option value="Psiquiatría de la discapacidad de aprendizaje">Psiquiatría de la discapacidad de aprendizaje</option>
                            <option value="Psiquiatría Forense">Psiquiatría Forense</option>
                            <option value="Psiquiatría geriátrica">Psiquiatría geriátrica</option>
                            <option value="Psiquiatría geriátrica / Psiquiatría antigua">Psiquiatría geriátrica / Psiquiatría antigua</option>
                            <option value="Psiquiatría Infantil y Adoliente">Psiquiatría Infantil y Adoliente</option>
                            <option value="Quiropráctico">Quiropráctico</option>
                            <option value="Radiología">Radiología</option>
                             <option value="Radiología diagnóstica">Radiología diagnóstica</option>
                            <option value="Radiología Intervencionista y Radiología Diagnóstica">Radiología Intervencionista y Radiología Diagnóstica</option> <option value="Radiología Nuclear">Radiología Nuclear</option>
                            <option value="Radiología Oral y Maxilofacial">Radiología Oral y Maxilofacial</option> <option value="Radiología pediátrica">Radiología pediátrica</option>
                            <option value="Radiología Vascular e Intervencionista">Radiología Vascular e Intervencionista</option> <option value="Reconstrucción de cabeza y cuello">Reconstrucción de cabeza y cuello</option>
                            <option value="Rehabilitación Cardiopulmonar">Rehabilitación Cardiopulmonar</option> <option value="Rehabilitacion Psiquiatria">Rehabilitacion Psiquiatria</option>
                            <option value="Rehabilitacion Quirurgica">Rehabilitacion Quirurgica</option> <option value="Rehabilitación reumatológica">Rehabilitación reumatológica</option>
                            <option value="Reumatología">Reumatología</option> <option value="Reumatología Pediátrica">Reumatología Pediátrica</option>
                            <option value="Rinología">Rinología</option> <option value="Salud infantil comunitaria">Salud infantil comunitaria</option>
                            <option value="Salud mental infantil">Salud mental infantil</option> <option value="Salud psicológica">Salud psicológica</option>
                            <option value="Salud Pública Dental">Salud Pública Dental</option> <option value="Salud pública y medicina comunitaria">Salud pública y medicina comunitaria</option>
                            <option value="Salud Pública y Medicina Preventiva General">Salud Pública y Medicina Preventiva General</option> <option value="Sexología">Sexología</option>
                            <option value="Teledermatologia">Teledermatologia</option> <option value="Toxicología">Toxicología</option>
                            <option value="Toxicología médica">Toxicología médica</option> <option value="Trauma Ortopédico">Trauma Ortopédico</option>
                            <option value="Tricología">Tricología</option> <option value="Uroginecología">Uroginecología</option>
                            <option value="Urología">Urología</option> <option value="Urología Pediátrica">Urología Pediátrica</option>
                            <option value="Urología reconstructiva">Urología reconstructiva</option> <option value="Uso indebido de sustancias Psiquiatría">Uso indebido de sustancias Psiquiatría</option>
                            <option value="Uveítis e inmunología ocular">Uveítis e inmunología ocular</option> <option value="Virología médica">Virología médica</option>
						</select>
						<div class="dropDownSelect2"></div>
					</div>
					<span class="focus-input100"></span>
				</div>



				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn">
						enviar
					</button>
				</div>

				<div class="contact100-form-social flex-c-m">
					<a href="https://www.facebook.com/midoctor24h" target="_blank" class="contact100-form-social-item flex-c-m bg1 m-r-5">
						<i class="fa fa-facebook-f" aria-hidden="true"></i>
					</a>

					<a href="https://www.instagram.com/midoctor24h/" target="_blank" class="contact100-form-social-item flex-c-m bg2 m-r-5">
						<i class="fa fa-instagram" aria-hidden="true"></i>
					</a>

				</div>
			</form>

            <div class="contact100-more flex-col-c-m" style="background-image: url({{ asset("images/bg-01.jpg") }});">
                <div class="info-text">
                    <div class="title">

                        <h3>
                            disfruta de una semana <span>gratis</span>
                        </h3>
                        <h4>
                            de tu propio <span>consultorio online</span>
                        </h4>

                    </div>
                    <div class="layer">
                        <h2>
							y con tu primer mes obten 50% de descuento
						</h2>
                    </div>
                </div>
                <div class="she">
                    <img src="{{ asset('images/she.png') }}" alt="">
                </div>
			</div>
		</div>
	</div>





<!--===============================================================================================-->
	<script src="{{ asset("vendor/jquery/jquery-3.2.1.min.js") }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset("vendor/animsition/js/animsition.min.js") }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset("vendor/bootstrap/js/popper.js") }}"></script>
	<script src="{{ asset("vendor/bootstrap/js/bootstrap.min.js") }}"></script>
<!--===============================================================================================-->
    <script src="{{ asset("vendor/select2/select2.min.js") }}"></script>

	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
		$(".js-select2").each(function(){
			$(this).on('select2:open', function (e){
				$(this).parent().next().addClass('eff-focus-selection');
			});
		});
		$(".js-select2").each(function(){
			$(this).on('select2:close', function (e){
				$(this).parent().next().removeClass('eff-focus-selection');
			});
		});

	</script>
<!--===============================================================================================-->
	<script src="{{ asset("vendor/daterangepicker/moment.min.js") }}"></script>
	<script src="{{ asset("vendor/daterangepicker/daterangepicker.js") }}"></script>
<!--===============================================================================================-->
    <script src="{{ asset("vendor/countdowntime/countdowntime.js") }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!--===============================================================================================-->
    <script src="{{ asset("js/main.js") }}"></script>


    @if (Session::has('success'))
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        Swal.fire({
            title: 'Exitoso',
            text: "{{ Session::get('success') }}",
            icon: 'success',
            confirmButtonText: 'Cool'
        })
    </script>
    @endif

    @if (Session::has('error'))
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        Swal.fire({
            title: 'Ha ocurrido un error',
            text: "{{ Session::get('error') }}",
            icon: 'error',
            confirmButtonText: 'Cool'
        })
    </script>
    @endif

</body>
</html>
