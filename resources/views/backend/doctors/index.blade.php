@extends('layouts.app')

@section('title', 'Lista de Doctores')
@section('nav.mails', 'active')

@section('head')
@endsection

@section('content')

<div class="container-fluid mt-4 mb-5">

    <div class="row">
        <div class="col-md-12">
                <h2 class="">Lista de personas que han contactado</h2>

                <div class="card shadow p-2">
                    <table class="table table-hover mt-3 dataTable">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Especialidad</th>
                                <th scope="col">País</th>
                                <th scope="col">Telefono</th>
                                <th scope="col">Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($doctors as $doctor)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $doctor->name }}</td>
                                    <td>{{ $doctor->email }}</td>
                                    <td>{{ $doctor->specialty }}</td>
                                    <td>{{ $doctor->country }}</td>
                                    <td>{{ $doctor->phone }}</td>
                                    <td>{{ $doctor->created_at->format('d/m/Y g:i a') }}</td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                    <div class="d-flex justify-content-center my-2">
                        {{ $doctors->links() }}
                    </div>
                </div>
        </div>
    </div>

</div>

@endsection
