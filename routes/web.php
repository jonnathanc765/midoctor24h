<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Artisan;

Route::get('/', function () {
    return view('index');
})->name('index');

Auth::routes();

Route::get('/register', function ()
{
    return abort(403);
});

Route::get('/home', function ()
{
    return redirect()->route('admin.doctors.index');
})->name('home');

Route::group(['middleware' => ['auth']], function () {


    Route::prefix('admin')->group(function ()
    {

        Route::prefix('doctors')->group(function ()
        {

            Route::get('/', 'DoctorController@index')->name('admin.doctors.index');

        });

    });


});

Route::prefix('doctors')->group(function ()
{

    Route::post('/store', 'DoctorController@store')->name('doctor.store');

});


// Route::get('/migrate-and-seed', function ()
// {

//     Artisan::call('migrate:fresh');
//     Artisan::call('db:seed');

//     return 'Migrado y llenado de tablas exitoso';

// });

// Route::get('/clear-cache', function ()
// {

//     Artisan::call('config:clear');

//     return 'Limpiado de cache listo';

// });
